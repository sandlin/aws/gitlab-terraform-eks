
# Store state in the project.
terraform {
  required_version = ">=1.0.0"
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/51126347/terraform/state/aws/"
    lock_address   = "https://gitlab.com/api/v4/projects/51126347/terraform/state/aws/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/51126347/terraform/state/aws/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}
